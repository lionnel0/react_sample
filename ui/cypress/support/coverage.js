/// <reference types="cypress"/>

const istanbul = require('istanbul-lib-coverage')

const map = istanbul.createCoverageMap({})

afterEach(function () {
    cy.window().then(window => {
        const coverage = window.__coverage__
        if (coverage) {
            map.merge(coverage)
        }
    })
})

Cypress.on('window:before:unload', e => {
    const coverage = e.currentTarget.__coverage__

    if (coverage) {
        map.merge(coverage)
    }
})

after(() => {
    cy.window().then(win => {
        const coverage = win.__coverage__

        if (coverage) {
            map.merge(coverage)
        }

        cy.writeFile(Cypress.env('TEST_REPORT_PATH') + '/out.json', JSON.stringify(map))
    })
})

