describe('Sample page loading', () => {

    const datacy = (id) => `[data-cy="${id}"]`

    it('Home page should have a ', () => {
        cy.visit('/')
        cy.get(datacy('SAMPLE')).should('be.visible')
    })

    it('Check disable behaviour', () => {
        cy.visit('/')
        cy.get('[id="RaisedButton"]').should('be.visible')
        cy.get('[id="RaisedButton"]').should('not.be.disabled')
        cy.get('[id="DisabledRaisedButton"]').should('be.visible')
        cy.get('[id="DisabledRaisedButton"]').should('not.be.disabled')
        cy.get('[id="RaisedButton"]').click()
        cy.get('[id="DisabledRaisedButton"]').should('be.disabled')
    })
})
