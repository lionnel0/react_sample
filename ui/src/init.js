import React from 'react'
import ReactDOM from 'react-dom'
import SampleComponent from './modules/sample/components/SampleComponent'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { Provider } from 'react-redux'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'

/**
 * Initialize the application with a specific module
 * @param {module} module: the module to initialize the application with
 */
const initFromModule = (module) => {

    if (!module) {
        console.log('*** module unknown ***')
        return
    }

    console.log('--- Loading module: ' + module.constants.MODULE_NAME + ' ---')
}


/**
 * Initialize the application
 * @param {modules} modules: list of modules to initialize the application with
 */
const init = (modules) => {

    /**
     * initialize from modules
     */
    modules.forEach(module => {
        initFromModule(module)
    })

    /**
     * then post initialize modules
     */
    modules.forEach(module => {
        // 1) is it defined module ? ( exception in case of : src\contributions\activities\sampleActivity\index.js)
        if (!module) {
            console.log('*** module is ' + module + ' ***')
        } else {
            console.log('*** ' + module.constants.MODULE_NAME + '.store() not found ***')
        }
    })

    return ReactDOM.render(
        <MuiThemeProvider muiTheme={getMuiTheme({})}>
            <Provider store={window.store}>
                <div>
                    <h1>Hello world</h1>
                    <SampleComponent></SampleComponent>
                </div>
            </Provider>
        </MuiThemeProvider>,
        document.getElementById('root')
    )
}

export default init
