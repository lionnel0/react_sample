import React from 'react'
import SampleComponent from '../SampleComponent'
import { shallow } from 'enzyme'

describe('Testing SampleComponent', () => {
    test('SampleComponent should render something', () => {
        let sampleComponent = shallow(<SampleComponent />)
        expect(sampleComponent).toBeDefined()
        sampleComponent.setProps({ message: 'This is a new test message.' })
    })
})
