import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'

class SampleComponent extends Component {

    constructor(props) {
        super(props)
        this.state = { disabled: false }
    }

    toggleDisabled() {
        this.setState({ disabled: !this.state.disabled })
    }

    untestedFunction() {
        return true
    }

    render() {
        let AcceptButton = ({ backgroundColor }) => (
            <RaisedButton
                buttonStyle={{ backgroundColor: backgroundColor }}
                primary={true}
                label="Accept"
                id="RaisedButton"
                onClick={e => this.toggleDisabled()}
            />)
        let AcceptButtonDisabled = () => (
            <RaisedButton
                primary={true}
                label="Accept"
                id="DisabledRaisedButton"
                disabled={this.state.disabled}
            />)

        return (
            <div
                open={this.state.open}
                message={this.props.message || ''}
                onRequestClose={this.handleRequestClose}
                class="sampleComponent" data-cy="SAMPLE">
                Sample component
                <p>
                    <AcceptButton></AcceptButton>
                    <p></p>
                    <AcceptButtonDisabled></AcceptButtonDisabled>
                </p>
            </div>
        )
    }
}

export default SampleComponent
