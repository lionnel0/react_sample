/**
 * Script has to pack and install core-ui locally to target destination path
 *
 * Dependency :
 * - "npm run build:lib" must be executed previously
 *
 * Required parameter :
 * - Target destination path
 *
 * During process :
 * - Check for valid target destination path ( existence of path, contains package.json )
 *
 **/

const fse = require('fs-extra')
const {spawnSync} = require('child_process')

const STATUS = {
    SUCCESS : 0,
    FAILED : 1
}

const LOG = {
    INFO   : 1,
    STATUS : 2,
    PROCESS: 3,
    ERROR  : 4
}

const log = (type, message) => {
    switch(type) {
        case LOG.INFO :
            console.log('[INFO] ' + message)
            break
        case LOG.STATUS :
            console.log('[STATUS] ' + message)
            break
        case LOG.PROCESS :
            console.log('[PROCESS] ' + message)
            break
        case LOG.ERROR:
            console.error('[ERROR] ' + message)
            process.exit(STATUS.FAILED)
            break
        default:
            console.log(message)
    }

}

const command = (commandType) => {
    let commandReturn = commandType
    if (isWin) {
        commandReturn += '.cmd'
    }
    return commandReturn
}

const changeDirTo = (directory) => {
    try {
        process.chdir(directory)
        log(LOG.INFO,'Change directory to : ' + process.cwd())
    } catch (err) {
        log(LOG.ERROR,'Change directory FAILED : ' + err)
    }
}

const npmTask = (parameters) => {
    log(LOG.PROCESS,'Npm task parameters : ' + JSON.stringify(parameters) + ' - Execute at location : ' + process.cwd())

    const resultNpmCommand = spawnSync(command('npm'), parameters)
    if (resultNpmCommand.status === STATUS.SUCCESS) {
        //process.stdout.write(resultNpmCommand.stdout)
        //process.stderr.write(resultNpmCommand.stderr)
        log(LOG.STATUS,'Npm task SUCCEEDED : parameters = ' + JSON.stringify(parameters))
    } else {
        process.stderr.write(resultNpmCommand.stderr)
        log(LOG.ERROR,'Npm task FAILED : parameters = ' + JSON.stringify(parameters))
    }
    return resultNpmCommand.status
}

const npmPack = () => {
    return npmTask(['pack'])
}

const npmInstall = (destination) => {
    return npmTask(['install', destination])
}

const checkParameters = () => {
    if (!parameterTargetDestinationPath) {
        log(LOG.ERROR,'Target location to install core-ui package is REQUIRED ')
    } else if (!fse.existsSync(parameterTargetDestinationPath)) {
        log(LOG.ERROR,'Target location is an INVALID PATH : ' + parameterTargetDestinationPath)
    } else if (!fse.existsSync(parameterTargetDestinationPath + '/package.json')) {
        log(LOG.ERROR,'Target location DOES NOT CONTAIN file : package.json')
    } else if (!fse.existsSync(packagePath)) {
        log(LOG.ERROR,'REQUIRE TO BUILD PROJECT : please execute before : "npm run build:lib"')
    }
    return true
}

const deleteFile = (file) => {
    if (fse.existsSync(file)) {
        log(LOG.PROCESS,'Delete previous file : ' + file)
        try {
            fse.removeSync(file)
            log(LOG.STATUS,'Deletion SUCCEEDED of file : ' + file)
        } catch (err) {
            log(LOG.ERROR,err)
        }
    }
}

const checkInstallation = () => {
    log(LOG.PROCESS,'Check installation')
    if ( fse.existsSync(nodeModuleDirectory)) {
        log(LOG.STATUS,'Check installation : DIRECTORY EXIST = ' + nodeModuleDirectory)
    } else {
        log(LOG.ERROR,'Check installation : DIRECTORY DOES NOT EXIST = ' + nodeModuleDirectory)
    }

    fse.readFile(packageJsonToCheck, function (err, data) {
        if (err) throw err
        if (data.indexOf(tgzFile) >= 0) {
            log(LOG.STATUS,'Check package.json : ' + packageJsonToCheck + ' CONTAINS THE STRING : ' + tgzFile )
            log(LOG.STATUS,'\n ==== INSTALLATION OK ==== \nFor ' + fullPathTgzFile + '\ninto ' + parameterTargetDestinationPath )
        } else {
            log(LOG.ERROR,'Check package.json : ' + packageJsonToCheck + ' DOES NOT CONTAINS the string : ' + tgzFile )
        }
    })
}

// Init all variables
const parameterTargetDestinationPath = process.argv[2]

const currentPath = process.cwd()
log(LOG.INFO,'Current path : ' + currentPath)
const packagePath = currentPath + '/es'
log(LOG.INFO,'Path to get package : ' + packagePath)
//
const packageName = require('./../package.json').name
log(LOG.INFO,'Package name : ' + packageName)
const packageVersion = require('./../package.json').version
log(LOG.INFO,'Package version : ' + packageVersion)
// Need to get OS type, because execution commands are different for Windows and Linux
const isWin = process.platform === 'win32'
log(LOG.INFO,'Is Windows OS = ' + isWin)
//
const tgzFile = packageName + '-' + packageVersion + '.tgz'
log(LOG.INFO,'File name create by "npm pack" : ' + tgzFile)
log(LOG.INFO,'Target destination path to install : ' + parameterTargetDestinationPath)
const fullPathTgzFile = packagePath + '/' + tgzFile
log(LOG.INFO,'Full path tarball file : ' + fullPathTgzFile)
const nodeModuleDirectory = parameterTargetDestinationPath + '/node_modules/' + packageName
log(LOG.INFO,'node module directory of installation : ' + nodeModuleDirectory)
const packageJsonToCheck = nodeModuleDirectory + '/package.json'
log(LOG.INFO,'package.json to check : ' + packageJsonToCheck)

// Process
if (checkParameters()) {
    // Have to delete previous tarball file, because npm pack concatenate with existing file
    deleteFile(tgzFile)
    changeDirTo(packagePath)
    //
    if ( npmPack() === STATUS.SUCCESS) {
        changeDirTo(parameterTargetDestinationPath)
        if (npmInstall(fullPathTgzFile) === STATUS.SUCCESS) {
            checkInstallation()
        }
    }
}