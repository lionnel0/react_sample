/* eslint-disable no-console */
const fs = require('fs')
const packageJson = require('../package.json')

let version = process.argv[2]
const gitCommit = process.env['GIT_COMMIT']
const buildNumber = process.env['BUILD_NUMBER']

console.log(`new version proposed: ${version}`)
if(typeof version !== 'string') throw new Error('no version provided or is not a string')

if(version.endsWith('-SNAPSHOT')){
    console.log(`add buildNumber: ${buildNumber} and gitCommit: ${gitCommit}`)

    version = `${version}.${gitCommit}.${buildNumber}`
}

console.log(`new version applied: ${version}`)
packageJson.version = version
fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 4))