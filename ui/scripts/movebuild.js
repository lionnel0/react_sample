/* eslint-disable no-console */
let path = require('path')
let mv = require('mv')
let mkdirp = require('mkdirp')
let fs = require('fs')

let destPath = path.dirname(require.main.filename) + '/../target/classes/static'

if (fs.existsSync(destPath)) {
    console.log('target/classes/static already exists')
} else {
    let srcPath = path.dirname(require.main.filename) + '/../build'

    console.log('Will move files from ' + srcPath + ' to ' + destPath)

    mkdirp.sync(destPath + '/..', function (err) {
        if (err)
            console.error(err)
        else
            console.log('folder created')
    })

    mv(srcPath, destPath, function (err) {
        if (err)
            console.error(err)
        else
            console.log('folder moved')
    })
}

