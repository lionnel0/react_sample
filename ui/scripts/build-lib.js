/* eslint-disable no-console */
/**
 * Add the 'package.json' to the lib
 */

let path = require('path')
let copyfiles  = require('copyfiles')

let destPath = path.join('es')

let copy = (src, dest) => {
    console.log(`Copying '${src}'...`)
    copyfiles([src, dest], function(error){
        if(error){
            console.error(error)
        }else{
            console.log(`'${src}' copied in: ${dest}`)
        }
    })
}

copy('package.json', destPath)
copy('.npmrc', destPath)