const path = require('path')

module.exports = function override(config, env) {
    const istanbul = {
        test: /\.(jsx|js)$/,
        include: path.resolve(__dirname, './src'),
        rules: [
            {
                loader: 'istanbul-instrumenter-loader',
                options: {
                    esModules: true,
                },
            },
        ],
    }

    config.module.rules.unshift(istanbul)

    return config
}