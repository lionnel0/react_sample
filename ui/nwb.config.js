//https://github.com/insin/nwb/blob/master/docs/Configuration.md
module.exports = {
    type: 'react-component',
    npm: {
        cjs: false,
        esModules: true,
        umd: false
    }
}
